package com.hrhx.springboot.mongodb.repository;

import com.hrhx.springboot.domain.Cnarea;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
/**
 * 
 * @author duhongming
 *
 */
public interface CnareaMongoRepository extends MongoRepository<Cnarea, Integer> {
	/**
	 * 获取相应区域级别信息
	 * @param level
	 * @return
	 */
	List<Cnarea> findByLevel(Integer level);
	/**
	 * 根据父级别获取相应区域级别信息
	 * @param level
	 * @param code
	 * @return
	 */
	List<Cnarea> findByLevelAndParentCode(Integer level,String code);
	
}
